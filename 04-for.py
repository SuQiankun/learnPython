#!/usr/bin/python3 
import sys
import string
'''
这里的 range 是一个内置函数, 生成一个从第一个数字 0 开始,到最后一个数字 5 结束的数字序列[0,1,2,3,4],

for循环就把这个数字序列中从第一个开始取出, 一次取出一个, 就是 代码中的 i ,

else 是可选的,如果 for循环中包含了 else 语句, 那么 else 总会在for 执行完成后执行, 除非在 for 循环中 break了
'''

for i in range(0,5):
    print(i)
else:
    print('打印结束啦 \n\n ')
    
'''
打印一份乘法口诀
一开始我们定义一个从 1 到 10 的数字序列 range(1,10) => (0,1,2,3,4,5,6,7,8,9)

第一行打印1个, 数字序列从 1 开始 , 到 2 结束;
第一行打印2个, 数字序列从 2 开始 , 到 3 结束;
....
在第二个循环中我们要比第一个循环中多一个可循环项, 所以我们第二个序列是 从 m 开始, 到 m+1 结束;

'''


for m in range(1, 10):
    for n in range(1, m+1):
        print("%d * %d = %d \t"%(n,m,n*m), end="")
    print(" \n ")


'''
一个漏斗算法:
每行输出奇数的符号, 各个行中心的符号对齐; 相邻行的符号相差两个(左边一个右边一个), 符号从多到少递减到1, 在从小到大递增到首尾两行相等;
eg: 
    假如你有 17 个 * 号;
    第一行放 5 个
    第二行放 3 个
    第三行放 1 个
    第四行放 3 个
    第五行放 5 个

        *****
         ***
          *
         ***
        *****



'''



a, b = input().split()
a = int(a) - 1
i = 3
num = 0
c=''
while a > 2 * i:
    num += 1
    a = a - 2 * i
    i += 2
num1 = num
 
for j in range(num):
    if j == 0:
        print(b * (2 * num + 1))
        c = b * (2 * num + 1)
    else:
        print(str(b * (2 * num + 1)).center(len(c)))
    num -= 1
print(b.center(len(c)))
for k in range(num1):
    print((b * (2 * (k + 1) + 1)).center(len(c)))
print(a)
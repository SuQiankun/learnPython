#!/usr/local/bin/python3 

'''

函数是一个可重用使用的程序片段, 你可以为一段特定功能的代码命名, 然后再其他地方使用这个命名的代码去处理特定的逻辑;

函数的定义使用 def , def 后面紧跟的是 函数名, 函数名后面会有一个 括号 (), 你可以在括号里面放入参数, 来使用参数或者操作参数做特定的逻辑;


'''

def say_hello(name):
    print ('你好哇,',name)

say_hello('两好三坏')


# 局部变量
#并不受函数内修改的影响
x = 50
def funx(x):
    print('x = ',x)
    x = 90
    print('修改后 x = ',x)

funx(x)
print ('函数外的 x = ',x)



# 全局变量
#
y = 20
def funcY():
    global y
    print('全局变量:',y)
    y = 88
    print('修改后全局变量:',y)

funcY()
print('函数外全局变量:',y)

# list 列表

tempList = ['许巍','李延亮','陈悦','张永光','刘效松']

print(tempList[0],len(tempList))
for i in tempList:
    print(i)

print('\n*******************\n')

# 增加元素
tempList.append('崔健')
for i in tempList:
    print(i)


# 删除元素
del tempList[len(tempList)-1]

print('\n*******************\n')

for i in tempList:
    print(i)




# Tuple  元组
rockBand =  ('唐朝','黑豹','超载','痛仰','飞乐队')
print(rockBand,len(rockBand))

print('\n*******************\n')

newRockBand = ('五月天','低苦艾',rockBand)
print(newRockBand,len(newRockBand))

print(newRockBand[2])
print('\n*******************\n')

# dict 字典

tempDict = {
    'name':'小小酥',
    'age':'26',
    'height':'90'
}

print(tempDict)

print('\n*******************\n')

for key,value in tempDict.items():
    # print(key,':',value)
    print('{} 是 {}'.format(key,value))

import os
import time

#备份文件路径
source = '/Users/su/Desktop/Masonry'
#备份目标路径
backURL = '/Users/su/Desktop/MasonryBack'

# 备份目标文件
target = backURL + os.sep + time.strftime('%Y%m%d%H%M%S') + '.zip'

# 不存在备份文件夹的话, 去创建
if not os.path.exists(backURL):
    os.mkdir(backURL)

# zip 打包
zip_command = 'zip -r {0} {1}' .format(target,''.join(source))

print(zip_command);

if os.system(zip_command) == 0:
    print('备份成功')
else:
    print('备份失败')